
using Weave

set_chunk_defaults!(
    :fig_width  => 6, 
    :fig_height => 2,
    :fig_pos => "h!"
)


#   .    1    .    2    .    3    .    4    .    5    .    6    .    7    .    8    .     92



filenombase = "makie"

BASE_DIR = @__DIR__

BUILD_DIR = "jmdout"
OUT_PATH = joinpath(BASE_DIR, BUILD_DIR)

`mkdir -p $(BUILD_DIR)` |> run



weave(
    joinpath(BASE_DIR, filenombase*".jmd"); 
    out_path=OUT_PATH,
    doctype="pandoc2html"
)




