
using Weave
===========

Hands on path to Julia literate programming through Julia Markdown (_.jmd_), processed by Weave.jl and compiled via Pandoc.  

Packages in play
----------------

1. DataFrames
1. Plots
1. RDatasets
1. Unitful
1. VegaDatasets
1. VegaLite
1. Pandoc

