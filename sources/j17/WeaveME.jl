
filenombase = "boundary-work"

BASE_PATH = @__DIR__
BUILD_DIR = "jmdout"
OUT_PATH  = joinpath(BASE_PATH, BUILD_DIR)

`mkdir -p $(BUILD_DIR)` |> run


using Weave

set_chunk_defaults!(
    :fig_width  => 6, 
    :fig_height => 2,
    :fig_pos => "h!"
)


#   .    1    .    2    .    3    .    4    .    5    .    6    .    7    .    8    .     92


# status 
println("Weaving...")


weave(
    joinpath(BASE_PATH, filenombase*".jmd"); 
    out_path=OUT_PATH,
    doctype="pandoc"
)


# status 
println("Woven...")


#   .    1    .    2    .    3    .    4    .    5    .    6    .    7    .    8    .     92


# status 
println("Pandoc...")


pandoc_arg_preamble = (    # need parens for awkward line breaks. begin block works too
    "preamble.tex" ∈ readdir(BASE_PATH) 
        ? "--include-in-header=$(joinpath(BASE_PATH,"preamble.tex"))" 
        : ""
)


# create a julia command object for the pandoc call

pandoc_call = 
    ```
    pandoc $(joinpath(OUT_PATH,filenombase * ".md"))
        output_pandoc.yml 
        --standalone  
        --resource-path=$(OUT_PATH)
        --pdf-engine=xelatex   
        --citeproc   
        --from=markdown   
        --to=latex   
        --output=$(filenombase * ".pdf")
    ```

#=
╭──                                                                                       ──╮
│ pandoc_call =      
│     ```
│     pandoc $(joinpath(OUT_PATH,filenombase * ".md"))  # the source to weave
│         output_pandoc.yml                             # format/typesetting
│         --standalone                                  # tells pandoc to use a template
│         --resource-path=$(OUT_PATH)                   # add to search path: images, etc
│         --pdf-engine=xelatex                          # typsetter
│         --citeproc                                    # for citations
│         --from=markdown                               # from pandoc markdown
│         --to=latex                                    # output format (pdf)
│         --output=$(filenombase * ".pdf")              # payoff
│     ```
│
╰── .    1    .    2    .    3    .    4    .    5    .    6    .    7    .    8    .    9=#

# status 
println("Pandoc command...\n")
println(pandoc_call)


# call pandoc
run(pandoc_call)




# d = get_chunk_defaults() 





