% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{ifxetex,ifluatex}
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
  \setmainfont[Numbers=Lowercase,Numbers=Proportional]{Lato}
  \setmonofont[]{MesloLGS NF}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  colorlinks=true,
  linkcolor=Maroon,
  filecolor=Maroon,
  citecolor=Blue,
  urlcolor=red,
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=1in]{geometry}
\usepackage{color}
\usepackage{fancyvrb}
\newcommand{\VerbBar}{|}
\newcommand{\VERB}{\Verb[commandchars=\\\{\}]}
\DefineVerbatimEnvironment{Highlighting}{Verbatim}{commandchars=\\\{\}}
% Add ',fontsize=\small' for more characters per line
\newenvironment{Shaded}{}{}
\newcommand{\AlertTok}[1]{\textcolor[rgb]{1.00,0.00,0.00}{\textbf{#1}}}
\newcommand{\AnnotationTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{#1}}}}
\newcommand{\AttributeTok}[1]{\textcolor[rgb]{0.49,0.56,0.16}{#1}}
\newcommand{\BaseNTok}[1]{\textcolor[rgb]{0.25,0.63,0.44}{#1}}
\newcommand{\BuiltInTok}[1]{#1}
\newcommand{\CharTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{#1}}
\newcommand{\CommentTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textit{#1}}}
\newcommand{\CommentVarTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{#1}}}}
\newcommand{\ConstantTok}[1]{\textcolor[rgb]{0.53,0.00,0.00}{#1}}
\newcommand{\ControlFlowTok}[1]{\textcolor[rgb]{0.00,0.44,0.13}{\textbf{#1}}}
\newcommand{\DataTypeTok}[1]{\textcolor[rgb]{0.56,0.13,0.00}{#1}}
\newcommand{\DecValTok}[1]{\textcolor[rgb]{0.25,0.63,0.44}{#1}}
\newcommand{\DocumentationTok}[1]{\textcolor[rgb]{0.73,0.13,0.13}{\textit{#1}}}
\newcommand{\ErrorTok}[1]{\textcolor[rgb]{1.00,0.00,0.00}{\textbf{#1}}}
\newcommand{\ExtensionTok}[1]{#1}
\newcommand{\FloatTok}[1]{\textcolor[rgb]{0.25,0.63,0.44}{#1}}
\newcommand{\FunctionTok}[1]{\textcolor[rgb]{0.02,0.16,0.49}{#1}}
\newcommand{\ImportTok}[1]{#1}
\newcommand{\InformationTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{#1}}}}
\newcommand{\KeywordTok}[1]{\textcolor[rgb]{0.00,0.44,0.13}{\textbf{#1}}}
\newcommand{\NormalTok}[1]{#1}
\newcommand{\OperatorTok}[1]{\textcolor[rgb]{0.40,0.40,0.40}{#1}}
\newcommand{\OtherTok}[1]{\textcolor[rgb]{0.00,0.44,0.13}{#1}}
\newcommand{\PreprocessorTok}[1]{\textcolor[rgb]{0.74,0.48,0.00}{#1}}
\newcommand{\RegionMarkerTok}[1]{#1}
\newcommand{\SpecialCharTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{#1}}
\newcommand{\SpecialStringTok}[1]{\textcolor[rgb]{0.73,0.40,0.53}{#1}}
\newcommand{\StringTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{#1}}
\newcommand{\VariableTok}[1]{\textcolor[rgb]{0.10,0.09,0.49}{#1}}
\newcommand{\VerbatimStringTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{#1}}
\newcommand{\WarningTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{#1}}}}
\usepackage{longtable,booktabs,array}
\usepackage{calc} % for calculating minipage widths
% Correct order of tables after \paragraph or \subparagraph
\usepackage{etoolbox}
\makeatletter
\patchcmd\longtable{\par}{\if@noskipsec\mbox{}\fi\par}{}{}
\makeatother
% Allow footnotes in longtable head/foot
\IfFileExists{footnotehyper.sty}{\usepackage{footnotehyper}}{\usepackage{footnote}}
\makesavenoteenv{longtable}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifluatex
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{}
\date{}

\begin{document}

\hypertarget{weave-play}{%
\section{weave play}\label{weave-play}}

Boys and girls, we will calculate some Julia stuff. 1234567890. really

\begin{verbatim}
    ❯ tree j17
    j17
    ├── 2021.0303_boundary-work.jl.ipynb
    ├── Manifest.toml
    ├── Project.toml
    ├── boundary-work.jmd
    ├── boundary-work.tex
    ├── jmdout
    │   ├── bdtest.pdf
    │   ├── boundary-work.md
    │   ├── figures
    │   │   ├── boundary-work_10_1.pdf
    │   │   ├── boundary-work_6_1.png
    │   │   ├── boundary-work_7_1.pdf
    │   │   ├── boundary-work_7_1.png
    │   │   ├── boundary-work_8_1.pdf
    │   │   ├── boundary-work_8_1.png
    │   │   └── boundary-work_9_1.pdf
    │   └── jmdout
    ├── output_pandoc.yml
    ├── output_weave.jl
    └── scratchpad
        ├── Untitled.ipynb
        ├── factorme.jl
        └── latex.tpl
\end{verbatim}

\begin{Shaded}
\begin{Highlighting}[]

\KeywordTok{using}\NormalTok{ Unitful}
 
\NormalTok{d }\OperatorTok{=} \FloatTok{50}\NormalTok{u}\StringTok{"mm"}
\NormalTok{A }\OperatorTok{=}\NormalTok{ π}\OperatorTok{/}\FloatTok{4}\OperatorTok{*}\NormalTok{d}\OperatorTok{\^{}}\FloatTok{2}
\NormalTok{P }\OperatorTok{=} \FloatTok{100}\NormalTok{u}\StringTok{"kPa"}

\NormalTok{F }\OperatorTok{=}\NormalTok{ P}\OperatorTok{*}\NormalTok{A }\OperatorTok{|\textgreater{}}\NormalTok{ u}\StringTok{"N"}\OperatorTok{;}
\end{Highlighting}
\end{Shaded}

The force was 196.34954 N big digits. The force was 196.35 N big
sigdigits.

\begin{quote}
tream
\end{quote}

link \href{https://pandoc.org/MANUAL.html\#creating-a-pdf}{pandoc link}

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{julia}\OperatorTok{\textgreater{}}\NormalTok{ weave(}\StringTok{"boundary{-}work.jmd"}\OperatorTok{;}\NormalTok{ out\_path }\OperatorTok{=} \StringTok{"jmdout"}\OperatorTok{,}\NormalTok{ doctype }\OperatorTok{=} \StringTok{"pandoc2pdf"}\OperatorTok{,}\NormalTok{ pandoc\_options}\OperatorTok{=}\NormalTok{[}\StringTok{""" {-}{-}variable "mainfontoptions: Numbers=Lowercase, Numbers=Proportional" """}
\end{Highlighting}
\end{Shaded}

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{weave(}\StringTok{"boundary{-}work.jmd"}\OperatorTok{;}\NormalTok{ out\_path }\OperatorTok{=} \StringTok{"jmdout"}\OperatorTok{,}\NormalTok{ doctype }\OperatorTok{=} \StringTok{"pandoc2pdf"}\OperatorTok{,}\NormalTok{ pandoc\_options}\OperatorTok{=}\NormalTok{[}\StringTok{"{-}{-}pdf{-}engine=xelatex"}\NormalTok{])}
\end{Highlighting}
\end{Shaded}

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{weave(}\StringTok{"boundary{-}work.jmd"}\OperatorTok{;}\NormalTok{ out\_path}\OperatorTok{=}\StringTok{"jmdout"}\OperatorTok{,}\NormalTok{ doctype}\OperatorTok{=}\StringTok{"pandoc"}\NormalTok{)}
\NormalTok{run(}\SpecialStringTok{\textasciigrave{}pandoc output.yml jmdout/boundary{-}work.md {-}o bdtest.pdf {-}{-}pdf{-}engine=xelatex\textasciigrave{}}\NormalTok{)}
\end{Highlighting}
\end{Shaded}

\hypertarget{examples}{%
\subsection{Examples}\label{examples}}

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
tristique nibh in aliquet vehicula. Sed convallis elementum vulputate.
Curabitur suscipit ac libero quis facilisis. Nullam volutpat cursus quam
tempus finibus. Vestibulum eleifend enim sit amet tellus tempus
imperdiet vel eu urna. Duis posuere iaculis massa, eget viverra nisl
consectetur id. Maecenas tristique vitae erat quis efficitur. Quisque
egestas tellus ac imperdiet volutpat. Sed in malesuada est. Nunc
bibendum imperdiet ipsum a commodo. Vestibulum hendrerit metus.

\hypertarget{sub-sub-heading}{%
\subsubsection{sub sub heading}\label{sub-sub-heading}}

Read the documentation:

\begin{itemize}
\tightlist
\item
  stable: \url{http://mpastell.github.io/Weave.jl/stable/}
\item
  latest: \url{http://weavejl.mpastell.com/dev/}
\end{itemize}

\hypertarget{block}{%
\subsubsection{block}\label{block}}

\hypertarget{mycode}{%
\label{mycode}}%
\begin{Shaded}
\begin{Highlighting}[numbers=left,,firstnumber=100,]
\NormalTok{qsort []     }\OtherTok{=}\NormalTok{ []}
\NormalTok{qsort (x}\OperatorTok{:}\NormalTok{xs) }\OtherTok{=}\NormalTok{ qsort (}\FunctionTok{filter}\NormalTok{ (}\OperatorTok{\textless{}}\NormalTok{ x) xs) }\OperatorTok{++}\NormalTok{ [x] }\OperatorTok{++}
\NormalTok{               qsort (}\FunctionTok{filter}\NormalTok{ (}\OperatorTok{\textgreater{}=}\NormalTok{ x) xs)}
\end{Highlighting}
\end{Shaded}

\hypertarget{how-about-a-md-table}{%
\subsubsection{how about a md table}\label{how-about-a-md-table}}

\begin{Shaded}
\begin{Highlighting}[numbers=left,,]

\NormalTok{    wgich     x     y}
\NormalTok{    {-}{-}{-}{-}{-}{-}{-} {-}{-}{-}{-}{-} {-}{-}{-}{-}{-}}
\InformationTok{    one       x}
\InformationTok{    two       x     18}
\InformationTok{    {-}{-}{-}{-}{-}{-}{-} {-}{-}{-}{-}{-} {-}{-}{-}{-}{-} }
\end{Highlighting}
\end{Shaded}

yields

\begin{longtable}[]{@{}lcc@{}}
\toprule
wgich & x & y \\
\midrule
\endhead
one & x & \\
two & x & 18 \\
\bottomrule
\end{longtable}

\hypertarget{lists-hear-ye-hear-ye}{%
\subsubsection{lists hear ye hear ye}\label{lists-hear-ye-hear-ye}}

\begin{Shaded}
\begin{Highlighting}[]
\SpecialStringTok{1. }\NormalTok{one}
\SpecialStringTok{1. }\NormalTok{two}
\SpecialStringTok{1. }\NormalTok{three}

\NormalTok{a. one}
\NormalTok{b. two}
\NormalTok{a. three}

\SpecialStringTok{1. }\NormalTok{one}
\NormalTok{\#. two}
\NormalTok{\#. three}
\NormalTok{   a. one}
\NormalTok{   \#. two}
\NormalTok{   \#. three}
\end{Highlighting}
\end{Shaded}

yields

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  one
\item
  two
\item
  three
\end{enumerate}

\begin{enumerate}
\def\labelenumi{\alph{enumi}.}
\tightlist
\item
  one
\item
  two
\item
  three
\end{enumerate}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  one
\item
  two
\item
  three

  \begin{enumerate}
  \def\labelenumii{\alph{enumii}.}
  \tightlist
  \item
    one
  \item
    two
  \item
    three
  \end{enumerate}
\end{enumerate}

\hypertarget{tabular-julia-dataframe}{%
\subsection{tabular julia dataframe}\label{tabular-julia-dataframe}}

\begin{Shaded}
\begin{Highlighting}[numbers=left,,]

\KeywordTok{using}\NormalTok{ DataFrames}
 
\NormalTok{df }\OperatorTok{=}\NormalTok{ DataFrame(}\OperatorTok{:}\NormalTok{one }\OperatorTok{=\textgreater{}}\NormalTok{ rand(}\FloatTok{5}\NormalTok{)}\OperatorTok{,} \OperatorTok{:}\NormalTok{two }\OperatorTok{=\textgreater{}}\NormalTok{ rand(}\FloatTok{5}\NormalTok{)}\OperatorTok{,} \OperatorTok{:}\NormalTok{three }\OperatorTok{=\textgreater{}}\NormalTok{ rand(}\FloatTok{5}\NormalTok{)) }
\PreprocessorTok{@show}\NormalTok{ df}
\OperatorTok{;}
\end{Highlighting}
\end{Shaded}

\begin{verbatim}
df = 5×3 DataFrame
 Row │ one       two        three
     │ Float64   Float64    Float64
─────┼────────────────────────────────
   1 │ 0.116831  0.927382   0.0323398
   2 │ 0.371846  0.409284   0.491382
   3 │ 0.183654  0.221316   0.0238075
   4 │ 0.147092  0.479816   0.596305
   5 │ 0.678965  0.0373086  0.498237
\end{verbatim}

\hypertarget{plots.jl}{%
\subsection{Plots.jl}\label{plots.jl}}

The basic code chunk will be run with default options and the code and
output will be captured.

then,

\begin{Shaded}
\begin{Highlighting}[numbers=left,,]
\InformationTok{\textasciitilde{}\textasciitilde{}\textasciitilde{}\textasciitilde{}\{.julia\}}
\InformationTok{using Plots}
\InformationTok{gr()}

\InformationTok{plot([sin,cos], 0, 2pi)}
\end{Highlighting}
\end{Shaded}

\begin{figure}
\centering
\includegraphics{figures/boundary-work_6_1.png}
\caption{figure caption as chunk option. look how blurry. need to force
vector output instead of raster}
\end{figure}

\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde\textasciitilde{}

yields

\begin{figure}
\centering
\includegraphics{figures/boundary-work_7_1.png}
\caption{figure caption as chunk option. look how blurry. need to force
vector output instead of raster}
\end{figure}

again, this time forcing pdf output.

\begin{verbatim}
```{julia; fig_ext=".pdf", fig_cap="vector output (pdf). look how clean and sharp", echo=false}
using Plots
gr()

plot([sin,cos], 0, 2pi)
```
\end{verbatim}

\begin{figure}
\centering
\includegraphics{figures/boundary-work_8_1.pdf}
\caption{vector output (pdf). look how clean and sharp}
\end{figure}

\hypertarget{vegalite}{%
\subsection{VegaLite}\label{vegalite}}

This code now produces a nice scatter plot. We specify these encodings
by using keyword arguments inside the (\textbf{vlplot?}) macro that
correspond to the names of the encoding channels, in our example the x
and y channel. We pass the names of the columns in our dataset that we
want to use for these channels as symbols, e.g.~as :Miles\_per\_Gallon
and :Horsepower.

\begin{Shaded}
\begin{Highlighting}[]
\KeywordTok{using}\NormalTok{ VegaLite}\OperatorTok{,}\NormalTok{ VegaDatasets}

\NormalTok{data }\OperatorTok{=}\NormalTok{ dataset(}\StringTok{"cars"}\NormalTok{)}

\NormalTok{p }\OperatorTok{=}\NormalTok{ dataset(}\StringTok{"cars"}\NormalTok{) }\OperatorTok{|\textgreater{}}
\PreprocessorTok{@vlplot}\NormalTok{(}
    \OperatorTok{:}\NormalTok{point}\OperatorTok{,}
\NormalTok{    x}\OperatorTok{=:}\NormalTok{Horsepower}\OperatorTok{,}
\NormalTok{    y}\OperatorTok{=:}\NormalTok{Miles\_per\_Gallon}\OperatorTok{,}
\NormalTok{    color}\OperatorTok{=:}\NormalTok{Origin}\OperatorTok{,}
\NormalTok{    width}\OperatorTok{=}\FloatTok{600}\OperatorTok{,}
\NormalTok{    height}\OperatorTok{=}\FloatTok{200}
\NormalTok{)}
\end{Highlighting}
\end{Shaded}

\begin{figure}
\centering
\includegraphics{figures/boundary-work_9_1.pdf}
\caption{vector output (pdf). look how clean and sharp}
\end{figure}

\hypertarget{repl}{%
\subsection{REPL}\label{repl}}

You can also control the way the results are captured, plot size etc.
using chunk options. Here is an example of a chunk that behaves like a
repl.

this is slick. key is the \texttt{term=true} chunk option.

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{x }\OperatorTok{=} \FloatTok{1}\OperatorTok{:}\FloatTok{10}
\NormalTok{d }\OperatorTok{=} \DataTypeTok{Dict}\NormalTok{(}\StringTok{"Weave"} \OperatorTok{=\textgreater{}} \StringTok{"testing"}\NormalTok{)}
\NormalTok{y }\OperatorTok{=}\NormalTok{ [}\FloatTok{2}\OperatorTok{,} \FloatTok{4} \OperatorTok{,}\FloatTok{8}\NormalTok{]}
\end{Highlighting}
\end{Shaded}

yields

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{julia}\OperatorTok{\textgreater{}}\NormalTok{ x }\OperatorTok{=} \FloatTok{1}\OperatorTok{:}\FloatTok{10}
\FloatTok{1}\OperatorTok{:}\FloatTok{10}

\NormalTok{julia}\OperatorTok{\textgreater{}}\NormalTok{ d }\OperatorTok{=} \DataTypeTok{Dict}\NormalTok{(}\StringTok{"Weave"} \OperatorTok{=\textgreater{}} \StringTok{"testing"}\NormalTok{)}
\DataTypeTok{Dict}\NormalTok{\{}\DataTypeTok{String}\OperatorTok{,}\DataTypeTok{String}\NormalTok{\} with }\FloatTok{1}\NormalTok{ entry}\OperatorTok{:}
  \StringTok{"Weave"} \OperatorTok{=\textgreater{}} \StringTok{"testing"}

\NormalTok{julia}\OperatorTok{\textgreater{}}\NormalTok{ y }\OperatorTok{=}\NormalTok{ [}\FloatTok{2}\OperatorTok{,} \FloatTok{4} \OperatorTok{,}\FloatTok{8}\NormalTok{]}
\FloatTok{3}\OperatorTok{{-}}\NormalTok{element }\DataTypeTok{Array}\NormalTok{\{}\DataTypeTok{Int64}\OperatorTok{,}\FloatTok{1}\NormalTok{\}}\OperatorTok{:}
 \FloatTok{2}
 \FloatTok{4}
 \FloatTok{8}
\end{Highlighting}
\end{Shaded}

hmmm\ldots{} https://github.com/jgm/pandoc/issues/4714 Could not fetch
Image-Resource for pdf and docx-conversion (html-conversion works fine)
\#4714

\end{document}
