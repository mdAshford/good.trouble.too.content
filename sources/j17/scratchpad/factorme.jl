

function factorme(num)
    f = factorme([num]) |> sort
    # return f[1:end-1] |> sort
    return f[2:end] #|> sort
end

##

function factorme(factors::Vector)
    println(factors)
    num = factors[end]
    (num == 1) && return factors

    iseven(num) &&
        begin
            pushfirst!(factors,2)
            factors[end] = num÷2
            factorme(factors)
        end


    divisor = length(factors)>1 ? max(factors[1],3) : 3

    while (factors[end] ≠ 1)
    # while (divisor ≤ sqrt(num))
        # println("num:",num," div:",divisor," factor",factors)
        if (num/divisor == num÷divisor)
            pushfirst!(factors,divisor)
            factors[end]=num÷divisor
            factorme(factors)
        else
            divisor = divisor ≤ sqrt(num) ? divisor + 2 : num
        end
    end
    return factors
end
