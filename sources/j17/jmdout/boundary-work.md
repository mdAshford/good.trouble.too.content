# weave play

Boys and girls, we will calculate some Julia stuff. 1234567890. really


        ❯ tree j17
        j17
        ├── 2021.0303_boundary-work.jl.ipynb
        ├── Manifest.toml
        ├── Project.toml
        ├── boundary-work.jmd
        ├── boundary-work.tex
        ├── jmdout
        │   ├── bdtest.pdf
        │   ├── boundary-work.md
        │   ├── figures
        │   │   ├── boundary-work_10_1.pdf
        │   │   ├── boundary-work_6_1.png
        │   │   ├── boundary-work_7_1.pdf
        │   │   ├── boundary-work_7_1.png
        │   │   ├── boundary-work_8_1.pdf
        │   │   ├── boundary-work_8_1.png
        │   │   └── boundary-work_9_1.pdf
        │   └── jmdout
        ├── output_pandoc.yml
        ├── output_weave.jl
        └── scratchpad
            ├── Untitled.ipynb
            ├── factorme.jl
            └── latex.tpl




~~~~{.julia}

using Unitful
 
d = 50u"mm"
A = π/4*d^2
P = 100u"kPa"

F = P*A |> u"N";
~~~~~~~~~~~~~




The force was 196.34954 N big digits.
The force was 196.35 N big sigdigits.

> tream

link [pandoc link](https://pandoc.org/MANUAL.html#creating-a-pdf)

~~~~{.julia}
julia> weave("boundary-work.jmd"; out_path = "jmdout", doctype = "pandoc2pdf", pandoc_options=[""" --variable "mainfontoptions: Numbers=Lowercase, Numbers=Proportional" """
~~~~~~~~~~~~~

~~~~{.julia}
weave("boundary-work.jmd"; out_path = "jmdout", doctype = "pandoc2pdf", pandoc_options=["--pdf-engine=xelatex"])
~~~~~~~~~~~~~

~~~~{.julia}
weave("boundary-work.jmd"; out_path="jmdout", doctype="pandoc")
run(`pandoc output.yml jmdout/boundary-work.md -o bdtest.pdf --pdf-engine=xelatex`)
~~~~~~~~~~~~~



## Examples

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique nibh in aliquet vehicula. Sed convallis elementum vulputate. Curabitur suscipit ac libero quis facilisis. Nullam volutpat cursus quam tempus finibus. Vestibulum eleifend enim sit amet tellus tempus imperdiet vel eu urna. Duis posuere iaculis massa, eget viverra nisl consectetur id. Maecenas tristique vitae erat quis efficitur. Quisque egestas tellus ac imperdiet volutpat. Sed in malesuada est. Nunc bibendum imperdiet ipsum a commodo. Vestibulum hendrerit metus.


### sub sub heading 

Read the documentation:

  - stable: [http://mpastell.github.io/Weave.jl/stable/](http://mpastell.github.io/Weave.jl/stable/)
  - latest: [http://weavejl.mpastell.com/dev/](http://weavejl.mpastell.com/dev/)


### block

``` {#mycode .haskell .numberLines startFrom="100"}
qsort []     = []
qsort (x:xs) = qsort (filter (< x) xs) ++ [x] ++
               qsort (filter (>= x) xs)
```

### how about a md table

``` {.md .numberLines}

    wgich     x     y
    ------- ----- -----
    one       x
    two       x     18
    ------- ----- ----- 
```

yields

wgich     x     y
------- ----- -----
one       x
two       x     18
------- ----- -----


### lists hear ye hear ye

```md
1. one
1. two
1. three

a. one
b. two
a. three

1. one
#. two
#. three
   a. one
   #. two
   #. three
```

yields

1. one
1. two
1. three

a. one
b. two
a. three

1. one
#. two
#. three
   a. one
   #. two
   #. three


## tabular julia dataframe

```{.julia .numberLines}

using DataFrames
 
df = DataFrame(:one => rand(5), :two => rand(5), :three => rand(5)) 
@show df
;
```

~~~~
df = 5×3 DataFrame
 Row │ one         two        three
     │ Float64     Float64    Float64
─────┼──────────────────────────────────
   1 │ 0.833982    0.173655   0.63617
   2 │ 0.0625964   0.0299831  0.592026
   3 │ 0.00221084  0.144389   0.673187
   4 │ 0.0334987   0.393798   0.0323848
   5 │ 0.811186    0.401108   0.243484
~~~~





## Plots.jl

The basic code chunk will be run with default options and the code and
output will be captured.

then, 

~~~~~~{.md .numberLines wrap=true}
~~~~{.julia}
using Plots
gr()

plot([sin,cos], 0, 2pi)
~~~~~~~~~~~~~

![figure caption as chunk option. look how blurry. need to force vector output instead of raster](figures/boundary-work_6_1.png)


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


yields


![figure caption as chunk option. look how blurry. need to force vector output instead of raster](figures/boundary-work_7_1.png)




again, this time forcing pdf output.


    ```{julia; fig_ext=".pdf", fig_cap="vector output (pdf). look how clean and sharp", echo=false}
    using Plots
    gr()

    plot([sin,cos], 0, 2pi)
    ```


![vector output (pdf). look how clean and sharp](figures/boundary-work_8_1.pdf)



## VegaLite

This code now produces a nice scatter plot. We specify these encodings by using keyword arguments inside the @vlplot macro that correspond to the names of the encoding channels, in our example the x and y channel. We pass the names of the columns in our dataset that we want to use for these channels as symbols, e.g. as :Miles_per_Gallon and :Horsepower.


~~~~{.julia}
using VegaLite, VegaDatasets

data = dataset("cars")

p = dataset("cars") |>
@vlplot(
    :point,
    x=:Horsepower,
    y=:Miles_per_Gallon,
    color=:Origin,
    width=600,
    height=200
)
~~~~~~~~~~~~~

![vector output (pdf). look how clean and sharp](figures/boundary-work_9_1.pdf)



## REPL

You can also control the way the results are captured, plot size etc.
using chunk options. Here is an example of a chunk that behaves like a repl.

this is slick. key is the `term=true` chunk option.

~~~~{.julia}
x = 1:10
d = Dict("Weave" => "testing")
y = [2, 4 ,8]
~~~~~~~~~~~~~



yields

~~~~{.julia}
julia> x = 1:10
1:10

julia> d = Dict("Weave" => "testing")
Dict{String,String} with 1 entry:
  "Weave" => "testing"

julia> y = [2, 4 ,8]
3-element Array{Int64,1}:
 2
 4
 8
~~~~~~~~~~~~~




hmmm...
https://github.com/jgm/pandoc/issues/4714
Could not fetch Image-Resource for pdf and docx-conversion (html-conversion works fine) #4714



