



using Modia
@model FirstOrder begin
  x = Variable(start=1)   # start means x(0)
  v = Variable(start=-1)   # start means x(0)
   T = Parameter(0.5)      # Time constant
   u = 0                 # Same as Parameter(2.0)
@equations begin
   v = der(x)
   T*der(v) + 0.1*v + 20*x = u        # der() means time derivative
   end
end;


using ModiaMath
result = simulate(FirstOrder, 20);
# @show result["x"][end];
# ModiaMath.plot(result, ("x","v"))


##

using Modia, ModiaMath

@model chemeq begin

   k1 = 0.4
   k2 = 0.6
   k3 = 0.3
   cA0 = 10
   cB0 = 5

   cA = Variable(start = cA0)
   cB = Variable(start = cB0)
   cC = Variable(start = 0)
   cD = Variable(start = 0)
   cE = Variable(start = 0)

@equations begin
   der(cA) =  -k1*cA*cB - 2*k3*cA^2
   der(cB) =  -k1*cA*cB
   der(cC) =   k1*cA*cB - k2*cC
   der(cD) =   k3*cA^2
   der(cE) = 2*k2*cC
   end
end;


result = simulate(chemeq, 5);
# @show result["cA"][end];
ModiaMath.plot(result, ("cA","cB","cC","cD","cE"))

##
using PGFPlotsX

time = result["time"] |> collect
cA = result["cA"]   |> collect
cB = result["cB"]   |> collect
cC = result["cC"]   |> collect
cD = result["cD"]   |> collect
cE = result["cE"]   |> collect

time


plot(time, data)

data = [cA cB cC cD cE]

##
using PGFPlotsX
@pgf TikzPicture(
        Axis(
            PlotInc({ only_marks },
                Table(; x = time, y = data))))


##

using Modia, ModiaMath, CoolProp

@model fixedvol begin

   V = 1       # m³
   T0 = 300.  # K
   P0 = 100000.  # Pa
   Plift = 300000.
   fluid = "Water"

   h0 = PropsSI("H","T",T0,"P",P0,fluid)  # kJ/kg
   u0 = PropsSI("U","T",T0,"P",P0,fluid)  # kJ/kg
   ρ0 = PropsSI("D","T",T0,"P",P0,fluid)  # kg/m³
   # ρlift = PropsSI("D","T",T0,"P",P0,fluid)  # kg/m³
   x0 = PropsSI("Q","T",T0,"P",P0,fluid)  # kg/m³
   # ρ = ρ0

   P = P0

   qin = 200.0  # W/kg

   h = Variable(start = h0)
   # u = Variable(start = u0)
   T = Variable(start = T0)
   # P = Variable(start = P0)
   x = Variable(start = x0)
   ρ = Variable(start = ρ0)
   # v = Variable(start = 1/ρ0)


@equations begin
   qin = der(h)
   T = PropsSI("T","H",h,"P",P,fluid)
   # P = PropsSI("P","H",h,"P",P,fluid)
   x = PropsSI("Q","H",h,"P",P,fluid)
   ρ = PropsSI("D","H",h,"P",P,fluid)
   end
end


result = simulate(fixedvol,13000);
@show result["time"][end];
ModiaMath.plot(result, [("T"),("x"),("ρ"),("P")])


##


function props(Plift,qin,T,P,ρ,u,h,x)
   if P < Plift
      qin = der(u)
      ρ = ρ0
      T = PropsSI("T","U",u,"D",ρ,fluid)
      P = PropsSI("P","U",u,"D",ρ,fluid)
      x = PropsSI("Q","U",u,"D",ρ,fluid)
      h = PropsSI("Q","U",u,"D",ρ,fluid)
   else
      qin = der(h)
      P = Plift
      T = PropsSI("T","H",h,"P",P,fluid)
      x = PropsSI("Q","H",h,"P",P,fluid)
      ρ = PropsSI("D","H",h,"P",P,fluid)
      u = PropsSI("U","H",h,"P",P,fluid)
   end


   (qin,T,P,ρ,u,h,x)

end

##

VERSION




##

using JuMP

# grid = [6 0 2 0 5 0 0 0 0;
#         0 0 0 0 0 3 0 4 0;
#         0 0 0 0 0 0 0 0 0;
#         4 3 0 0 0 8 0 0 0;
#         0 1 0 0 0 0 2 0 0;
#         0 0 0 0 0 0 7 0 0;
#         5 0 0 2 7 0 0 0 0;
#         0 0 0 0 0 0 0 8 1;
#         0 0 0 6 0 0 0 0 0]


  grid = [6 0 0 0 0 2 0 3 0;
          0 0 0 1 0 0 0 0 0;
          8 0 0 0 6 7 0 0 4;
          4 0 0 0 9 0 3 0 8;
          0 0 0 0 0 0 0 0 0;
          0 0 3 0 0 0 0 1 0;
          0 0 5 0 3 0 4 0 0;
          7 0 9 0 8 4 0 0 0;
          0 0 4 0 0 5 0 8 9]

using ConstraintSolver
const CS = ConstraintSolver

# creating a constraint solver model and setting ConstraintSolver as the optimizer.
m = Model(CS.Optimizer)
# define the 81 variables
@variable(m, 1 <= x[1:9,1:9] <= 9, Int)
# set variables if fixed
for r=1:9, c=1:9
    if grid[r,c] != 0
        @constraint(m, x[r,c] == grid[r,c])
    end
end

for rc = 1:9
    @constraint(m, x[rc,:] in CS.AllDifferentSet())
    @constraint(m, x[:,rc] in CS.AllDifferentSet())
end

for br=0:2
    for bc=0:2
        @constraint(m, vec(x[br*3+1:(br+1)*3,bc*3+1:(bc+1)*3]) in CS.AllDifferentSet())
    end
end

optimize!(m)

# retrieve grid
grid = convert.(Int, JuMP.value.(x))


##






x


##


using JuMP, ConstraintSolver
const CS = ConstraintSolver

m = Model(CS.Optimizer)
@variable(m, 1 <= x <= 9, Int)
@variable(m, 1 <= y <= 5, Int)

@constraint(m, x + y == 14)

optimize!(m)
status = JuMP.termination_status(m)


##

@show convert.(Integer,JuMP.value.(x))
@show convert.(Integer,JuMP.value.(y))
